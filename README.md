# ttn-crown
CROn + spaWN. Rudimentary process manager based on Cron and written in Bash.

## Install
Clone this repo (or copy/download the [script](https://gitlab.com/ttanttakun/ttn-crown/raw/master/crown.sh) and make it executable: `chmod +x crown.sh`)

## Config
`ttn-crown` is configured with a text file. A new line matching this template per process:

`<NAME>;<COMMAND>;<SLEEP>`

* NAME is the string used to check (`ps ax | grep ...`) if the process is running
* COMMAND is what is going to be executed if the process is not active
* SLEEP, seconds to sleep after COMMAND execution

Be aware that there are no comments nor blank lines.
This is the config at **Ttanttakun**:

```
qjackctl;/usr/bin/qjackctl -s -p=TTN -a=/home/server/ttn.xml;15
jackeq;/usr/bin/jackeq /home/server/eq;5
aras-player;/home/server/aras/aras-player/bin/aras-player /home/server/aras/aras.conf;1
darkice;/usr/bin/darkice -c /home/server/darkice.cfg;1
```

## Run
Edit your crontab (`crontab -e`) and add these two lines:
```
@reboot cd /path/to/ttn-crown/ && bash crown.sh crown.config >> crown.log
* * * * * cd /path/to/ttn-crown/ && bash crown.sh crown.config >> crown.log
```

`ttn-crown` will run on boot and launch your processes. It will run every minute and do what is needed if something fails.

## Notes
* `ttn-crown` is super basic, but is really easy to setup, configure and run.
* Exports a display (line 11, `export DISPLAY=:0.0`), maybe it's inappropriate for you.
* Logs each supplied command (change line 19 to `$2 &` if you don't need that).
* After years in *production* it will be replaced by Systemd services. Farewell little `ttn-crown`!
