#!/bin/bash

function quit {
	echo "Conf needed. Aborting."
	exit
}

[ "$#" -eq 1 ] || quit

echo "CHECK: "$(date)
export DISPLAY=:0.0

function check {
	if ps ax | grep -v grep |  grep $1 > /dev/null
		then
			echo $1":on"
		else
			echo $1":off -- restarting"
			$2 >> $1.log &
			echo "sleeping for "$3
			sleep $3
	fi
}

while IFS='' read -r line || [[ -n "$line" ]]; do
	IFS=';' read -ra ARR <<< "$line"
	check ${ARR[0]} "${ARR[1]}" ${ARR[2]}
done < "$1"

exit
